Bug report
-----------------------------------------------------

**Version:** (Insert the version used here or the Git commit ID)

**Summary:** (Insert a summary of the bug you encountered, keep it short)

**Expected Behaviour:** (Insert the behaviour you expected from the software here)

**Actual Behaviour:** (Insert here what actually happened)

**Steps to reproduce:**

(Insert here how to reproduce the bug)

1. Step 1
2. Step 2
3. Step 3

**Further useful details:**

(Insert other useful details here, be as precise as possible,
don't insert Stack Backtraces here or long error messages, instead zip
and attach them)

**I already tried to:**

- [ ] Make a clean install of the product (deleted all software-related files/directories)

**Final notes:**
If the program has a configuration file, please attach it, so to make tracing the issue easier